﻿using Microsoft.Research.CommunityTechnologies.Treemap;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

/* This is the main form for the FileTreeDisplay
 * 
 * This code is free to use under the CodeProject Open License
 * Attribution would be appreciated.
 * 
 * (C)2009 Scott Leckie, kwolo.com
 * 
 */

namespace FileTreeView
{
    /// <summary>
    /// Main form - prompts for a folder and then displays the TreeMap for it
    /// </summary>
    public partial class FileTreeDisplayForm : Form
    {
        #region Private fields
        
        // The FileTree is over written every time we press GO
        // and is populated with the contents of the specified folder
        private FileTree fileTree;

        // This object is used to synchronise with the FileTree population
        // thread. It passes back progress information and allows us to 
        // pass a cancel request back to the called thread.
        private FileTree.FileTreeWorkerThreadInfo workerThreadInfo;

        // The thread that the FileTree population runs on. This is set back
        // to null when the thread completes
        private Thread workerThread;

        // We run a timer tick very second, while we are populating the FileTree
        // object. When we realise that population has finished, we do a bit of
        // tidying up, cancel the timer and populate the TreeMap
        private readonly System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();

        // We can right click on a node and present a context menu. These are two
        // separate operations; right clicking causes us to remember the node that
        // was click on, and launches the menu. Then the menu handlers need to 
        // know which node to operate on, which is stored here
        private Node nodeWeClickedOn;

        // A collector of labels are created that act like buttons. Each has a
        // background colour set to a specific colour and an event handler to
        // set the display threshold on the TreeMap
        private List<Label> colourClickButtons;
        #endregion

        #region Constructors and Destructors
        public FileTreeDisplayForm()
        {
            InitializeComponent();
            SetTreemapProperties(treemapControl);
            fileInformationLabel.Text = "";
            cancelButton.Enabled = false;

            // Node label depth tracker
            textDisplayTracker.Minimum = 0;
            textDisplayTracker.Maximum = 7;         // Steps 1 .. 5, plus 0 for none and 6 for all
            textDisplayTracker.Value = textDisplayTracker.Maximum;

            // Right click context menu
            nodeMenuExplore.Click += nodeMenuExplore_Click;
            nodeMenuOpen.Click += nodeMenuOpen_Click;
            nodeMenuZoomIn.Click += nodeMenuZoomIn_Click;
            nodeMenuZoomOut.Click += nodeMenuZoomOut_Click;

            // Timer handler for when we are processing files in the FileTree object
            timer.Interval = 1000;
            timer.Tick += timer_Tick;

            // Build up the array of labels that will act as colour selection buttons
            SetupColorButtons();
        }
        #endregion

        #region Form setup and initialisation code
        /// <summary>
        /// We create 7 brand new labels, each of which looks like a
        /// mini coloured button. They are arrayed from white through
        /// to black with some lovely shades in between. They each
        /// have a click event handler pointing to the same routine
        /// which fetches the colour of the button that was clicked 
        /// and sets the TreeMaps colour threshold to that.
        /// These labels are stored in the FlowPanel so we don't need
        /// to worry about positioning.
        /// </summary>
        private void SetupColorButtons()
        {
            colourClickButtons = new List<Label>();

            for (var i=0; i<7; i++)
            {
                var l = new Label
                {
                    AutoSize = false,
                    Width = 20,
                    Height = 10,
                    BorderStyle = BorderStyle.Fixed3D
                };
                colourClickButtons.Add(l);
                colourFlowPanel.Controls.Add(l);
                l.Click += colourButton_Click;
            }

            colourClickButtons[0].BackColor = Color.White;
            colourClickButtons[1].BackColor = Color.Yellow;
            colourClickButtons[2].BackColor = Color.Red;
            colourClickButtons[3].BackColor = Color.Green;
            colourClickButtons[4].BackColor = Color.Blue;
            colourClickButtons[5].BackColor = Color.Gray;
            colourClickButtons[6].BackColor = Color.Black;
        }
        #endregion

        #region Main form buttons event handlers
        /// <summary>
        /// We clicked on a label (pretending to be a button)
        /// so see what colour it is, and set the outer colour
        /// threshold of the TreeMap to this.
        /// All TreeMap colours except black have black text.
        /// If the TreeMap is set to black then its text is red
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">EventArgs</param>
        private void colourButton_Click(object sender, EventArgs e)
        {
            treemapControl.MaxColor = ((Label)sender).BackColor;
            if (treemapControl.MaxColor == Color.Black)
            {
                treemapControl.FontSolidColor = Color.Red;
                treemapControl.BorderColor = Color.Gray;
            }
            else
            {
                treemapControl.FontSolidColor = Color.Black;
                treemapControl.BorderColor = Color.Black;
            }
        }

        /// <summary>
        /// The user pressed the go button so empty the TreeMap,
        /// enable the timer, create a new WorkerThread and start it.
        /// The new thread will start to populate the FileTree object
        /// and the timer tick will fire every second and;
        /// 1) Update the progress counters
        /// 2) See whether the thread has completed
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">EventArgs</param>
        private void goButton_Click(object sender, EventArgs e)
        {
            // Start the timer running every second
            timer.Enabled = true;

            // Empty the TreeMap so the user doesn't click on it while we are busy
            treemapControl.Clear();

            // Ensure that there is no "old" node being remembered before we populate
            nodeWeClickedOn = null;

            // Create a new ThreadInfo helper object that we will check during the timer
            // tick, then create the new thread ready to run
            workerThreadInfo = new FileTree.FileTreeWorkerThreadInfo();
            workerThread = new Thread(WorkerThreadFunction);

            // Button state change - we can cancel, but we can't re-go
            cancelButton.Enabled = true;
            goButton.Enabled = false;
            
            // Hide the error controls
            lblErrors.Visible = cmdShowErrors.Visible = false;

            // And.... GO! Worker thread will now process the folders over
            // the next few seconds, minutes or hours
            workerThread.Start();
        }

        /// <summary>
        /// The worker thread is presumably running, and the user pressed Cancel
        /// so signal the thread to stop by setting the stop flag on the shared
        /// FileTreeWorkerThreadInfo object
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">EventArgs</param>
        private void cancelButton_Click(object sender, EventArgs e)
        {
            if (workerThreadInfo != null)
                workerThreadInfo.StopRequested = true;
        }

        /// <summary>
        /// User has pressed the "..." select a folder button
        /// If they select a decent folder, fill the folder input
        /// box and set focus to the GO! button
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">EventArgs</param>
        private void openFolderButton_Click(object sender, EventArgs e)
        {
            var dlg = new FolderBrowserDialog
            {
                Description = @"Select folder to trawl",
                RootFolder = Environment.SpecialFolder.MyComputer,
                ShowNewFolderButton = false
            };

            if (dlg.ShowDialog() != DialogResult.OK)
                return;

            folderInput.Text = dlg.SelectedPath;
            goButton.Focus();
        }     
        #endregion

        #region Worker thread function that populates the tree map and the tick event handler
        /// <summary>
        /// This worker function is launched on a separate thread.
        /// All it does is populate a new FileTree object, which can take
        /// a few seconds or many minutes. The FileTree population is
        /// relatively safe (any anticipated file access exceptions are
        /// caught within itself) but, just in case, the whole call is
        /// wrapped in a try/catch block that will report an error back
        /// on failure.
        /// </summary>
        private void WorkerThreadFunction()
        {
            try
            {
                fileTree = new FileTree(folderInput.Text, workerThreadInfo);
                // Will run for a while, populating the file tree structure
            }
            catch (Exception ex)
            {
                workerThreadInfo.ErrorMessage = ex.Message;
                workerThreadInfo.Success = false;
            }
        }
        
        /// <summary>
        /// This handler is fired every second while we are populating the
        /// FileTree object. We can be in two states;
        /// <para>1) Thread is alive and running, in which case update
        /// the counters</para>
        /// <para>2) Thread has ended, so start unravelling the results
        /// and disable the timer tick</para>
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">EventArgs</param>
        private void timer_Tick(object sender, EventArgs e)
        {
            // If the thread has completed, tidy up and populate the TreeMap
            if (workerThread == null || !workerThread.IsAlive)
            {
                timer.Enabled = false;
                cancelButton.Enabled = false;
                goButton.Enabled = true;
                workerThread = null;
                cmdShowErrors.Visible = workerThreadInfo.ErrorList.ErrorCount > 0;
                if (workerThreadInfo.Success)
                    PopulateTreeMap(fileTree);
                else
                    MessageBox.Show(workerThreadInfo.ErrorMessage, @"Failed to retrieve directory details");
            }
            
            // Thread is still running so let the user know we're making some progress
            fileInformationLabel.Text = string.Format(@"Processed {0} folders and {1} files, total {2} in {3} seconds",
                workerThreadInfo.NumFolders, workerThreadInfo.NumFiles, workerThreadInfo.TotalFileSizeFormatted,
                (int) workerThreadInfo.ElapsedTime.TotalSeconds);

            if (workerThreadInfo.ErrorList.ErrorCount > 0)
            {
                lblErrors.Visible = true;
                lblErrors.Text = string.Format(@"{0} errors...", workerThreadInfo.ErrorList.ErrorCount);
            }
        }
        #endregion

        #region Populate treemap view with contents of the FileTree object
        /// <summary>
        /// We should have a valid FileTree object so populate the TreeMap
        /// <para>The TreeMap is first cleared, and we call the population
        /// method. This is wrapped in a BeginUpdate...EndUpdate pair
        /// to tell the TreeMap not to update itself while it is being
        /// filled</para>
        /// </summary>
        /// <param name="tree">A populated FileTree object</param>
        /// <exception cref="ArgumentNullException">
        /// FileTree must not be null</exception>
        private void PopulateTreeMap(FileTree tree)
        {
            if (tree == null)
                throw new ArgumentNullException("tree");

            treemapControl.BeginUpdate();
            treemapControl.Clear();
            nodeWeClickedOn = null;
            FillTreeMapWithFileTree(tree);
            treemapControl.EndUpdate();
        }

        /// <summary>
        /// Populate the TreeMap with a FileTree collection of entries
        /// <para>This method iteratively walks the FileTree and adds
        /// entries to the TreeMap, ensuring that child folders and 
        /// files are represented within nodes corresponding to their
        /// parent folder.</para>
        /// <para>Each node has its tag set to the FileTreeEntry
        /// that it refers to, which helps when clicking on any of
        /// the nodes</para>
        /// </summary>
        /// <param name="tree">A populated FileTree object</param>
        private void FillTreeMapWithFileTree(FileTree tree)
        {
            var iterator = 0;

            if (tree == null || tree.ToList().Count < 1)
                return;             // Nothing to draw

            var foldersList = new List<FileTreeEntry>
            {
                tree.ToList()[0]
            };

            while (iterator < foldersList.Count)
            {
                var folder = foldersList[iterator];

                var parentNodeSet = iterator == 0 ? treemapControl.Nodes : ((Node) folder.Parent.Tag).Nodes;

                var thisFolderNode = parentNodeSet.Add(folder.Name, folder.Size, 33F + folder.FolderDepth *50);

                folder.Tag = thisFolderNode;        // Save this folder's node ref to itself
                thisFolderNode.Tag = folder;        // And store the ref to the FileTreeEntry in the node
                thisFolderNode.ToolTip = string.Format("Folder: {0} {1}", folder.Name, folder.SizeFormatted);

                foreach (var entry in folder.Contents)
                {
                    if (entry.Type == FileTreeEntry.EntryType.Folder)
                    {
                        foldersList.Add(entry);
                    }
                    else
                    {
                        var file = thisFolderNode.Nodes.Add(new Node(entry.Name, entry.Size,-150F));
                        file.Tag = entry;
                        file.ToolTip = string.Format("File: {0} {1}", entry.Name, entry.SizeFormatted);
                    }
                }
                iterator++;
            }
        }
        #endregion

        #region Event handler for the slider label that varies depth of label text
        /// <summary>
        /// The user can use the slider to select to which depth the nodes labels
        /// will be displayed. For example, we may want every single node to display
        /// its label (which is the file/folder name) but this can become cluttered.
        /// Moving this slider tells the TreeMap to only show labels on nodes from
        /// a specific depth.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">EventArgs</param>
        private void textDisplayTracker_ValueChanged(object sender, EventArgs e)
        {
            // Extremes of the scale are None(0) or All (7)
            if (textDisplayTracker.Value == textDisplayTracker.Minimum)
            {
                treemapControl.NodeLevelsWithText = NodeLevelsWithText.None;
                return;
            }
            if (textDisplayTracker.Value == textDisplayTracker.Maximum)
            {
                treemapControl.NodeLevelsWithText = NodeLevelsWithText.All;
                return;
            }

            // A gradient between 1 and 6 was selected. The Nodes Level selection 
            // range on the TreeMap starts at zero so just set it to our
            // selection minus one
            treemapControl.NodeLevelsWithText = NodeLevelsWithText.Range;
            treemapControl.SetNodeLevelsWithTextRange(0, textDisplayTracker.Value - 1);
        }
        #endregion

        #region Treemap setup and event handlers
        /// <summary>
        /// Some one off properties for the TreeMap
        /// </summary>
        /// <param name="oTreeMapControl">A TreeMapControl</param>
        // ReSharper disable once IdentifierTypo
        private void SetTreemapProperties(TreemapControl oTreeMapControl)
        {
            oTreeMapControl.NodeLevelsWithText = NodeLevelsWithText.All;
            oTreeMapControl.NodeColorAlgorithm = NodeColorAlgorithm.UseColorMetric;
            oTreeMapControl.MinColorMetric = -200F;
            oTreeMapControl.MaxColorMetric = 200F;

            oTreeMapControl.MinColor = Color.BlanchedAlmond;
            oTreeMapControl.MaxColor = Color.Gray;

            oTreeMapControl.IsZoomable = true;

            oTreeMapControl.ContextMenuStrip = nodeMenuStrip;
        }

        /// <summary>
        /// User double clicked on a node so zoom in. This is quite
        /// easy as the TreeMap control directly supports this method
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="nodeEventArgs">NodeEventArgs</param>
        private void TreeMapControl_NodeDoubleClick(object sender, NodeEventArgs nodeEventArgs)
        {
            if (treemapControl.CanZoomIn(nodeEventArgs.Node))
                treemapControl.ZoomIn(nodeEventArgs.Node);
        }

        /// <summary>
        /// MouseUp event on one of the nodes. We're interested in two buttons here;
        /// <para>XButton1 (not present on all mice) which is the back-pedal button
        /// on most web browsers. Here, we use it to zoom out.</para>
        /// <para>Right button - display the context menu, but remember which node
        /// was selected so that the menu handlers can use it later</para>
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="nodeMouseEventArgs">NodeMouseEventArgs</param>
        private void treeMapControl_NodeMouseUp(object sender, NodeMouseEventArgs nodeMouseEventArgs)
        {
            // Remember for a later menu event
            nodeWeClickedOn = nodeMouseEventArgs.Node;

            // XButton1 is the browser back-pedal, so we zoom out here
            // ReSharper disable once ConvertIfStatementToSwitchStatement
            if (nodeMouseEventArgs.Button == MouseButtons.XButton1)
            {
                if (treemapControl.CanZoomOut())
                    treemapControl.ZoomOut();
                return;
            }

            // Right click. The context menu will display automatically, but
            // we enable / disable the zoom in/out options depending on whether
            // the node is able to do this
            if (nodeMouseEventArgs.Button != MouseButtons.Right) 
                return;
            
            nodeMenuZoomIn.Enabled = treemapControl.CanZoomIn(nodeWeClickedOn);
            nodeMenuZoomOut.Enabled = treemapControl.CanZoomOut();
        }
        #endregion

        #region Treemap node menu handlers
        /// <summary>
        /// User tries to zoom in to a node. This is by double clicking
        /// or selecting from the context menu and we act on the node that
        /// was remembered in the MouseUp event handler.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">EventArgs</param>
        private void nodeMenuZoomIn_Click(object sender, EventArgs e)
        {
            if (nodeWeClickedOn != null && treemapControl.CanZoomIn(nodeWeClickedOn))
                treemapControl.ZoomIn(nodeWeClickedOn);
        }
        
        /// <summary>
        /// User wants to zoom out. We can just let the TreeMap do this -
        /// no need to remember which node we were at
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">EventArgs</param>
        private void nodeMenuZoomOut_Click(object sender, EventArgs e)
        {
            if (treemapControl.CanZoomOut())
                treemapControl.ZoomOut();
        }

        /// <summary>
        /// User wants to open the current node from the context menu.
        /// <para>All we do is remember the node, and pass its 
        /// complete path to Windows. Hopefully, Windows knows what
        /// to do with it...</para>
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">EventArgs</param>
        private void nodeMenuOpen_Click(object sender, EventArgs e)
        {
            if (nodeWeClickedOn == null || nodeWeClickedOn.Tag == null)
                return;

            var cmd = "";

            try
            {
                // The nodes tag contains a reference to the original FileTreeEntry
                // so get that back and find out the path to the object we want to open
                var entry = (FileTreeEntry)nodeWeClickedOn.Tag;
                cmd = entry.FullPath;

                Process.Start(cmd);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + @" while trying to run " + cmd, @"Failed to open file or folder");
            }
        }

        /// <summary>
        /// The user wants to explore this node. We should have remembered
        /// which node they clicked on (via the MouseUp handler) so retrieve
        /// that node, find the FileTreeEntry and pass that as an argument
        /// to Explorer.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">EventArgs</param>
        private void nodeMenuExplore_Click(object sender, EventArgs e)
        {
            if (nodeWeClickedOn == null || nodeWeClickedOn.Tag == null)
                return;

            var cmdArgs = "";

            try
            {
                var entry = (FileTreeEntry)nodeWeClickedOn.Tag;
                cmdArgs = string.Format("/select,\"{0}\"", entry.FullPath);

                Process.Start("explorer", cmdArgs);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + @" while trying to run Explorer " + cmdArgs, @"Failed to start Explorer");
            }
        }
        #endregion

        private void cmdShowErrors_Click(object sender, EventArgs e)
        {
            var errorForm = new ErrorListForm(workerThreadInfo.ErrorList);
            errorForm.ShowDialog();
        }
    }
}
