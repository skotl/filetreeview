﻿using System;
using System.Collections.Generic;

/* This is the FileTreeEntry class, which describes a folder
 * or file.
 * Every folder or file, apart from the "root folder" (see
 * FileTree.cs) has a parent folder, so this is also a linked
 * list.
 * 
 * This code is free to use under the CodeProject Open License
 * Attribution would be appreciated.
 * 
 * (C)2009 Scott Leckie, kwolo.com
 * 
 */

namespace FileTreeView
{
    /// <summary>
    /// Describes a folder or a file
    /// </summary>
    public class FileTreeEntry
    {
        #region Constants and enums

        // Constants used by the pretty SizeFormatted property to 
        // decide what unit to report back in
        private const long Kilobyte = 1024;
        private const long Megabyte = 1024 * 1024;
        private const long Gigabyte = Megabyte * 1024;

        /// <summary>
        /// Each <see cref="FileTreeEntry"/> object can be either
        /// a file or a folder. Note that some properties are only
        /// valid for one, or other, of these types
        /// </summary>
        public enum EntryType
        {
            File,
            Folder
        }

        #endregion

        #region Private fields

        private readonly List<FileTreeEntry> contents;
        private readonly string name;
        private string path;
        private int folderDepth;
        private readonly EntryType type;

        #endregion

        #region Public properties

        /// <summary>
        /// The short name of this file or folder (for example; data.csv for c:\data\excel\data.csv)
        /// </summary>
        public string Name
        {
            get { return name; }
        }

        /// <summary>
        /// The size of this file or folder
        /// </summary>
        public long Size { get; private set; }

        /// <summary>
        /// Defines whether this is a file or a folder
        /// </summary>
        public EntryType Type
        {
            get { return type; }
        }

        /// <summary>
        /// The parent folder of this file or folder
        /// <para>Null if this is the root folder</para>
        /// </summary>
        public FileTreeEntry Parent { get; private set; }

        /// <summary>
        /// Caller settable object
        /// <para>Not used within this class</para>
        /// </summary>
        public object Tag { get; set; }


        /// <summary>
        /// The prettified version of the Size property
        /// <para>Returns a string in KB, MB or GB format</para>
        /// </summary>
        public string SizeFormatted
        {
            get
            {
                if (Size < Megabyte)
                    return string.Format("{0}KB", Size / Kilobyte);

                return Size / Megabyte < 4000
                    ? string.Format("{0}MB", Size / Megabyte)
                    : string.Format("{0}GB", Size / Gigabyte);
            }
        }

        /// <summary>
        /// If this is a folder, returns the (readonly) list of folders
        /// and/or files that it contains
        /// </summary>
        /// <exception cref="InvalidOperationException">
        /// Contents are only valid if this is a folder
        /// </exception>
        public IEnumerable<FileTreeEntry> Contents
        {
            get
            {
                if (Type == EntryType.Folder)
                    return contents.AsReadOnly();

                throw new InvalidOperationException("You cannot retrieve the Contents of a File, only of a Folder");
            }
        }

        /// <summary>
        /// The path of this entry's folder (excluding its name)
        /// For the full path to this entry use the FullPath property
        /// </summary>
        private string Path
        {
            get { return Type == EntryType.Folder ? path : Parent.Path; }
        }

        /// <summary>
        /// Returns the full path to this file or folder
        /// </summary>
        public string FullPath
        {
            get { return Type == EntryType.Folder ? Path : Path + "\\" + Name; }
        }

        /// <summary>
        /// Indicates the depth of folder that this entry is in, 
        /// relative to its root folder.
        /// <para>For example, searching from c:\, the file c:\data\excel\test.xls
        /// has a FolderDepth of 2</para>
        /// </summary>
        public int FolderDepth
        {
            get { return Type == EntryType.Folder ? folderDepth : Parent.folderDepth; }
        }

        #endregion

        #region Constructors and destructors

        /// <summary>
        /// Create a new FileTreeEntry
        /// <para>This is intended only to be called from member functions</para>
        /// </summary>
        /// <param name="type">Type of entry</param>
        /// <param name="name">Short name of entry</param>
        private FileTreeEntry(EntryType type, string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("Must pass a valid name");

            this.type = type;
            this.name = name;

            if (this.type == EntryType.Folder)
            {
                contents = new List<FileTreeEntry>();
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Create a root folder. The only difference between a root folder
        /// and a non-root one is that the root folder does not have a parent
        /// folder.
        /// <para>Note, also, that you can only add folders or files to
        /// existing folders, so you should start be creating a root
        /// folder and then add to the object created by it</para>
        /// </summary>
        /// <param name="name">Short name of the folder</param>
        /// <param name="path">Path of the folder</param>
        /// <returns>A new <see cref="FileTreeEntry"/> that is the root folder</returns>
        public static FileTreeEntry CreateRootFolder(string name, string path)
        {
            if (string.IsNullOrEmpty(path))
                throw new ArgumentException("Must pass a valid path");

            var f = new FileTreeEntry(EntryType.Folder, name)
            {
                path = path
            };

            return f;
        }

        /// <summary>
        /// Add a new folder to an existing folder.
        /// <para>This folder will be added to the Contents collection
        /// of its parent folder, and will have its FolderDepth property
        /// set to the parent folder FolderDepth + 1</para>
        /// <para>The parent of the new folder will be the folder that
        /// this was created within</para>
        /// </summary>
        /// <param name="fileName">Short name of the folder to create</param>
        /// <param name="filePath">The path to the folder</param>
        /// <returns>A new <see cref="FileTreeEntry"/> entry for the new folder</returns>
        /// <exception cref="InvalidOperationException">
        /// You are trying to add a folder to a file. You can only add to a folder.</exception>
        /// <exception cref="ArgumentException">
        /// The path is empty or null</exception>
        public FileTreeEntry AddFolder(string fileName, string filePath)
        {
            if (type != EntryType.Folder)
                throw new InvalidOperationException("Cannot add a Folder to a File");
            if (string.IsNullOrEmpty(filePath))
                throw new ArgumentException("Must pass a valid path");

            var f = new FileTreeEntry(EntryType.Folder, fileName)
            {
                path = filePath, Parent = this
            };
            
            contents.Add(f);
            f.folderDepth = folderDepth + 1;

            return f;
        }

        /// <summary>
        /// Add a new file to an existing folder.
        /// <para>This file will be added to the Contents collection
        /// of its parent folder.</para>
        /// <para>The parent of the new file will be the folder that
        /// this was created within</para>
        /// <para>The size of this folder will be saved, and will also
        /// be added to all of the parent folders, ensuring that any
        /// folder has an accurate record of the total size of its contents</para>
        /// </summary>
        /// <param name="fileName">Short name of the file to create</param>
        /// <param name="fileSize">The size of the file</param>
        /// <returns>A new <see cref="FileTreeEntry"/> entry for the new file</returns>
        /// <exception cref="InvalidOperationException">
        /// You are trying to add a file to a file. You can only add to a folder.</exception>
        public FileTreeEntry AddFile(string fileName, long fileSize)
        {
            if (type != EntryType.Folder)
                throw new InvalidOperationException("Cannot add a File to a File");

            var f = new FileTreeEntry(EntryType.File, fileName)
            {
                Size = fileSize, Parent = this
                
            };
            contents.Add(f);
            fileSize += fileSize; // Add the file's size to this folder's size, but also to all of the parents
            var folderParent = Parent;
            while (folderParent != null)
            {
                folderParent.Size += fileSize;
                folderParent = folderParent.Parent;
            }

            return f;
        }

        #endregion
    }
}