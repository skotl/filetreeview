﻿using System;

namespace FileTreeView
{
    public class FileOrDirectoryException : Exception
    {
        public string EntryPath { get; set; }

        public FileOrDirectoryException(string entryPath, Exception innerException)
            : base(innerException.Message, innerException)
        {
            EntryPath = entryPath;
        }

        public override string ToString()
        {
            return string.Format("{0} - {1} {2}", InnerException.GetType().Name, InnerException.Message, EntryPath);
        }
    }
}