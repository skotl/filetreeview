﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

/* This is the FileTree class, which contains a collection of
 * FileTreeEntry objects. 
 * It is instantiated by calling the constructor with details
 * of the path you want to trawl, along with a reference to
 * a helper object that is used for thread synchronisation.
 * When the constructor returns, the FileTree contains a 
 * collection of FileTreeEntry objects within its "Contents"
 * property.
 * Note that you can ask the thread to stop processing folders
 * by setting the StopRequested flag in the helper object
 * passed to the constructor.
 * 
 * This code is free to use under the CodeProject Open License
 * Attribution would be appreciated.
 * 
 * (C)2009 Scott Leckie, kwolo.com
 * 
 */

namespace FileTreeView
{
    /// <summary>
    /// Describes a tree of folders and files, populated from a specific root directory
    /// <para>See <see cref="FileTreeEntry"/> for details of the contents</para>
    /// </summary>
    public class FileTree
    {
        #region FileTreeWorkerThreadInfo helper class for synchronisation

        /// <summary>
        /// This is a helper object passed to the FileTree constructor.
        /// The primary goal of this class is to synchronise a calling 
        /// thread (probably a UI) with the thread that calls the FileTree
        /// constructor. Use this class to observe progress while the
        /// FileTree is being built and/or to cancel the process.
        /// </summary>
        public class FileTreeWorkerThreadInfo
        {
            // Constants used by the pretty total size property
            private const long Megabyte = 1024 * 1024;
            private const long Gigabyte = Megabyte * 1024;

            /// <summary>
            /// Number of files processed so far (or in total, if the constructor completed)
            /// </summary>
            public long NumFiles;

            /// <summary>
            /// Number of folders processed so far (or in total, if the constructor completed)
            /// </summary>
            public long NumFolders;

            /// <summary>
            /// Total size of all files processed so far (or in total, if the constructor completed)
            /// </summary>
            public ulong TotalFileSize;

            /// <summary>
            /// Set StopRequested to true from the calling thread if you want to abort the FileTree processing
            /// </summary>
            public bool StopRequested = false;

            /// <summary>
            /// This is not used by the FileTree, however can be used by the worker thread to 
            /// store any messages that should be passed to the UI thread
            /// </summary>
            public string ErrorMessage = "";

            /// <summary>
            /// Set to true if the constructor successfully processes the requested folder structure
            /// with no fatal errors
            /// </summary>
            public bool Success;

            /// <summary>
            /// The amount of time it took to process the request
            /// </summary>
            public TimeSpan ElapsedTime { get; set; }

            public ErrorList ErrorList { get; private set; }

            public FileTreeWorkerThreadInfo()
            {
                ErrorList = new ErrorList();    
            }
            
            /// <summary>
            /// Returns a pretty version of the total size of all files so far
            /// </summary>
            public string TotalFileSizeFormatted
            {
                get
                {
                    return TotalFileSize / Megabyte < 4000
                        ? string.Format("{0}MB", TotalFileSize / Megabyte)
                        : string.Format("{0}GB", TotalFileSize / Gigabyte);
                }
            }
        }

        #endregion

        #region Private fields

        // The Dictionary allows us to easily find a folder object, based on its path
        private readonly Dictionary<string, FileTreeEntry> listOfFolders;

        // This is the collection of every single sub-folder and file under the starting path
        private readonly List<FileTreeEntry> listOfEntries;

        // Ref to the WorkerThreadInfo object passed to the constructor
        private readonly FileTreeWorkerThreadInfo workerThreadInfo;

        #endregion

        #region Constructors / Destructors

        
        /// <summary>
        /// Instantiates a new FileTree object and populates it with the entire
        /// folder structure underneath the specified starting path
        /// <para>A calling thread can check progress by referring to 
        /// the counters in the passed <see cref="FileTreeWorkerThreadInfo"/>
        /// object. You can also signal the FileTree building by setting a
        /// stop flag in the <see cref="FileTreeWorkerThreadInfo"/></para>
        /// </summary>
        /// <param name="startingPath">Path to build the FileTree from</param>
        /// <param name="threadInfo">A <see cref="FileTreeWorkerThreadInfo"/> object used to synchronise threads</param>
        /// <exception cref="ArgumentNullException">
        /// Null or empty startingPath or threadInfo
        /// </exception>
        public FileTree(string startingPath, FileTreeWorkerThreadInfo threadInfo)
        {
            if (string.IsNullOrEmpty(startingPath))
                throw new ArgumentNullException("startingPath");
            if (threadInfo == null)
                throw new ArgumentNullException("threadInfo");

            workerThreadInfo = threadInfo; // Used to communicate with a calling thread

            listOfFolders = new Dictionary<string, FileTreeEntry>();
            listOfEntries = new List<FileTreeEntry>();

            var rootFolder = FileTreeEntry.CreateRootFolder(startingPath, startingPath);

            listOfFolders.Add(startingPath, rootFolder);
            listOfEntries.Add(rootFolder);

            workerThreadInfo.NumFolders++;

            // Do all the hard work. Continues until completed folder traversal, or asked
            // to stop by setting the threadInfo.StopRequested flag
            ProcessFolder(startingPath);
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Returns the complete list of <see cref="FileTreeEntry"/> objects
        /// discovered under the starting folder.
        /// </summary>
        /// <returns>Read only list of <see cref="FileTreeEntry"/> objects</returns>
        public IList<FileTreeEntry> ToList()
        {
            return listOfEntries.AsReadOnly();
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Iteratively build the FileTree with all of the folders and files we find
        /// under the starting path. This is an iterative search, rather than a
        /// recursive one, because recursive searching on large folders will cause
        /// a stack overflow. In theory, this technique is limited only by memory.
        /// </summary>
        /// <param name="startingPath">Path to start searching from</param>
        private void ProcessFolder(string startingPath)
        {
            var iterator = 0;
            var dirList = new List<string> {startingPath};
            var lastEntry = "";
            var timer = new Stopwatch();
            timer.Start();

            // Every new folder found is added to the list to be searched. Continue until we have
            // found, and reported on, every folder or the calling thread wants us to stop
            while (iterator < dirList.Count && !workerThreadInfo.StopRequested)
            {
                var parentFolder = dirList[iterator];

                try
                {
                    foreach (var dir in Directory.GetDirectories(dirList[iterator]))
                    {
                        lastEntry = dir;
                        AddFolder(parentFolder, dir, dir);
                        dirList.Add(dir);
                    }

                    foreach (var filename in Directory.GetFiles(dirList[iterator]))
                    {
                        lastEntry = filename;
                        var file = new FileInfo(filename);
                        AddFile(parentFolder, file.Name, file.Length);
                    }
                }
                // There are two *acceptable* exceptions that we may see, but should not consider fatal
                catch (Exception ex)
                {
                    workerThreadInfo.ErrorList.Add(new FileOrDirectoryException(lastEntry, ex));
                }

                iterator++;
                if (timer.Elapsed - workerThreadInfo.ElapsedTime > new TimeSpan(0, 0, 0, 1))
                    workerThreadInfo.ElapsedTime = timer.Elapsed;
            }

            workerThreadInfo.Success = true;
            workerThreadInfo.ElapsedTime = timer.Elapsed;
        }

        /// <summary>
        /// Creates a new <see cref="FileTreeEntry"/> folder object, based
        /// on the info provided in name and path, and adds it to the
        /// <see cref="FileTreeEntry"/> object that is the parent folder.
        /// <para>The parent folder is looked up by checking the dictionary
        /// of previously discovered folders for a matching path. The parent
        /// must already have been created!</para>
        /// </summary>
        /// <param name="parent">Path of the parent folder</param>
        /// <param name="name">Name of the folder to be added</param>
        /// <param name="path">Full path of the folder to ne added (parent + name)</param>
        /// <returns>The <see cref="FileTreeEntry"/> of the new folder</returns>
        /// <exception cref="ArgumentException">
        /// One of the parent, name or path parameters is null or empty
        /// </exception>
        /// <exception cref="KeyNotFoundException">
        /// The <see cref="FileTreeEntry"/> referred to by the parent 
        /// parameter does not refer to a previously discovered folder
        /// </exception>
        private void AddFolder(string parent, string name, string path)
        {
            if (string.IsNullOrEmpty(parent))
                throw new ArgumentException("Must supply a parent folder");
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("Must supply a name");
            if (string.IsNullOrEmpty(path))
                throw new ArgumentException("Must supply a path");

            var parentFolder = listOfFolders[parent];

            var newFolder = parentFolder.AddFolder(name, path);

            listOfFolders.Add(path, newFolder);
            listOfEntries.Add(newFolder);

            workerThreadInfo.NumFolders++;
        }

        /// <summary>
        /// Creates a new <see cref="FileTreeEntry"/> file object, based
        /// on the info provided in name, and adds it to the
        /// <see cref="FileTreeEntry"/> object that is the parent folder.
        /// <para>The parent folder is looked up by checking the dictionary
        /// of previously discovered folders for a matching path. The parent
        /// must already have been created!</para>
        /// </summary>
        /// <param name="parent">Path of the parent folder</param>
        /// <param name="name">Name of the file to be added</param>
        /// <param name="size">Size of the file to be added</param>
        /// <returns>The <see cref="FileTreeEntry"/> of the new file</returns>
        /// <exception cref="ArgumentException">
        /// One of the parent or name parameters is null or empty
        /// </exception>
        /// <exception cref="KeyNotFoundException">
        /// The <see cref="FileTreeEntry"/> referred to by the parent 
        /// parameter does not refer to a previously discovered folder
        /// </exception>
        private void AddFile(string parent, string name, long size)
        {
            if (string.IsNullOrEmpty(parent))
                throw new ArgumentException("Must supply a parent folder");
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("Must supply a name");

            var parentFolder = listOfFolders[parent];

            var newFile = parentFolder.AddFile(name, size);

            listOfEntries.Add(newFile);

            workerThreadInfo.NumFiles++;
            workerThreadInfo.TotalFileSize += (ulong) size;
        }

        #endregion
    }
}