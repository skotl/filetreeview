﻿namespace FileTreeView
{
    partial class FileTreeDisplayForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FileTreeDisplayForm));
            this.goButton = new System.Windows.Forms.Button();
            this.folderInput = new System.Windows.Forms.TextBox();
            this.cancelButton = new System.Windows.Forms.Button();
            this.treemapControl = new Microsoft.Research.CommunityTechnologies.Treemap.TreemapControl();
            this.fileInformationLabel = new System.Windows.Forms.Label();
            this.textDisplayTracker = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.nodeMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.nodeMenuExplore = new System.Windows.Forms.ToolStripMenuItem();
            this.nodeMenuOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.nodeMenuZoomIn = new System.Windows.Forms.ToolStripMenuItem();
            this.nodeMenuZoomOut = new System.Windows.Forms.ToolStripMenuItem();
            this.colourFlowPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.openFolderButton = new System.Windows.Forms.Button();
            this.lblErrors = new System.Windows.Forms.Label();
            this.cmdShowErrors = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.textDisplayTracker)).BeginInit();
            this.nodeMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // goButton
            // 
            this.goButton.Location = new System.Drawing.Point(251, 13);
            this.goButton.Name = "goButton";
            this.goButton.Size = new System.Drawing.Size(75, 23);
            this.goButton.TabIndex = 1;
            this.goButton.Text = "&Go";
            this.goButton.UseVisualStyleBackColor = true;
            this.goButton.Click += new System.EventHandler(this.goButton_Click);
            // 
            // folderInput
            // 
            this.folderInput.Location = new System.Drawing.Point(13, 13);
            this.folderInput.Name = "folderInput";
            this.folderInput.Size = new System.Drawing.Size(178, 20);
            this.folderInput.TabIndex = 0;
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(251, 42);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 2;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // treemapControl
            // 
            this.treemapControl.AllowDrag = false;
            this.treemapControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.treemapControl.BorderColor = System.Drawing.SystemColors.WindowFrame;
            this.treemapControl.DiscreteNegativeColors = 20;
            this.treemapControl.DiscretePositiveColors = 20;
            this.treemapControl.EmptySpaceLocation = Microsoft.Research.CommunityTechnologies.Treemap.EmptySpaceLocation.DeterminedByLayoutAlgorithm;
            this.treemapControl.FontFamily = "Arial";
            this.treemapControl.FontSolidColor = System.Drawing.SystemColors.WindowText;
            this.treemapControl.IsZoomable = false;
            this.treemapControl.LayoutAlgorithm = Microsoft.Research.CommunityTechnologies.Treemap.LayoutAlgorithm.BottomWeightedSquarified;
            this.treemapControl.Location = new System.Drawing.Point(13, 90);
            this.treemapControl.MaxColor = System.Drawing.Color.Green;
            this.treemapControl.MaxColorMetric = 100F;
            this.treemapControl.MinColor = System.Drawing.Color.Red;
            this.treemapControl.MinColorMetric = -100F;
            this.treemapControl.Name = "treemapControl";
            this.treemapControl.NodeColorAlgorithm = Microsoft.Research.CommunityTechnologies.Treemap.NodeColorAlgorithm.UseColorMetric;
            this.treemapControl.NodeLevelsWithText = Microsoft.Research.CommunityTechnologies.Treemap.NodeLevelsWithText.All;
            this.treemapControl.PaddingDecrementPerLevelPx = 1;
            this.treemapControl.PaddingPx = 5;
            this.treemapControl.PenWidthDecrementPerLevelPx = 1;
            this.treemapControl.PenWidthPx = 3;
            this.treemapControl.SelectedBackColor = System.Drawing.SystemColors.Highlight;
            this.treemapControl.SelectedFontColor = System.Drawing.SystemColors.HighlightText;
            this.treemapControl.ShowToolTips = true;
            this.treemapControl.Size = new System.Drawing.Size(877, 310);
            this.treemapControl.TabIndex = 4;
            this.treemapControl.TextLocation = Microsoft.Research.CommunityTechnologies.Treemap.TextLocation.Top;
            this.treemapControl.NodeMouseUp += new Microsoft.Research.CommunityTechnologies.Treemap.TreemapControl.NodeMouseEventHandler(this.treeMapControl_NodeMouseUp);
            this.treemapControl.NodeDoubleClick += new Microsoft.Research.CommunityTechnologies.Treemap.TreemapControl.NodeEventHandler(this.TreeMapControl_NodeDoubleClick);
            // 
            // fileInformationLabel
            // 
            this.fileInformationLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.fileInformationLabel.Location = new System.Drawing.Point(332, 16);
            this.fileInformationLabel.Name = "fileInformationLabel";
            this.fileInformationLabel.Size = new System.Drawing.Size(352, 20);
            this.fileInformationLabel.TabIndex = 5;
            this.fileInformationLabel.Text = "fileInformationLabel";
            // 
            // textDisplayTracker
            // 
            this.textDisplayTracker.Location = new System.Drawing.Point(612, 38);
            this.textDisplayTracker.Maximum = 7;
            this.textDisplayTracker.Name = "textDisplayTracker";
            this.textDisplayTracker.Size = new System.Drawing.Size(104, 45);
            this.textDisplayTracker.TabIndex = 6;
            this.textDisplayTracker.ValueChanged += new System.EventHandler(this.textDisplayTracker_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(543, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Label depth";
            // 
            // nodeMenuStrip
            // 
            this.nodeMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { this.nodeMenuExplore, this.nodeMenuOpen, this.nodeMenuZoomIn, this.nodeMenuZoomOut });
            this.nodeMenuStrip.Name = "nodeMenuStrip";
            this.nodeMenuStrip.Size = new System.Drawing.Size(188, 92);
            // 
            // nodeMenuExplore
            // 
            this.nodeMenuExplore.Name = "nodeMenuExplore";
            this.nodeMenuExplore.Size = new System.Drawing.Size(187, 22);
            this.nodeMenuExplore.Text = "Explore";
            // 
            // nodeMenuOpen
            // 
            this.nodeMenuOpen.Name = "nodeMenuOpen";
            this.nodeMenuOpen.Size = new System.Drawing.Size(187, 22);
            this.nodeMenuOpen.Text = "Open";
            // 
            // nodeMenuZoomIn
            // 
            this.nodeMenuZoomIn.Name = "nodeMenuZoomIn";
            this.nodeMenuZoomIn.Size = new System.Drawing.Size(187, 22);
            this.nodeMenuZoomIn.Text = "Zoom In (dbl-click)";
            // 
            // nodeMenuZoomOut
            // 
            this.nodeMenuZoomOut.Name = "nodeMenuZoomOut";
            this.nodeMenuZoomOut.Size = new System.Drawing.Size(187, 22);
            this.nodeMenuZoomOut.Text = "Zoom Out (x-button)";
            // 
            // colourFlowPanel
            // 
            this.colourFlowPanel.Location = new System.Drawing.Point(335, 39);
            this.colourFlowPanel.Name = "colourFlowPanel";
            this.colourFlowPanel.Size = new System.Drawing.Size(202, 25);
            this.colourFlowPanel.TabIndex = 8;
            // 
            // openFolderButton
            // 
            this.openFolderButton.Location = new System.Drawing.Point(198, 13);
            this.openFolderButton.Name = "openFolderButton";
            this.openFolderButton.Size = new System.Drawing.Size(47, 23);
            this.openFolderButton.TabIndex = 9;
            this.openFolderButton.Text = "...";
            this.openFolderButton.UseVisualStyleBackColor = true;
            this.openFolderButton.Click += new System.EventHandler(this.openFolderButton_Click);
            // 
            // lblErrors
            // 
            this.lblErrors.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblErrors.Location = new System.Drawing.Point(690, 16);
            this.lblErrors.Name = "lblErrors";
            this.lblErrors.Size = new System.Drawing.Size(118, 23);
            this.lblErrors.TabIndex = 10;
            this.lblErrors.Text = "label2";
            this.lblErrors.Visible = false;
            // 
            // cmdShowErrors
            // 
            this.cmdShowErrors.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdShowErrors.Location = new System.Drawing.Point(824, 10);
            this.cmdShowErrors.Name = "cmdShowErrors";
            this.cmdShowErrors.Size = new System.Drawing.Size(75, 23);
            this.cmdShowErrors.TabIndex = 11;
            this.cmdShowErrors.Text = "&Show...";
            this.cmdShowErrors.UseVisualStyleBackColor = true;
            this.cmdShowErrors.Visible = false;
            this.cmdShowErrors.Click += new System.EventHandler(this.cmdShowErrors_Click);
            // 
            // FileTreeDisplayForm
            // 
            this.AcceptButton = this.goButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(902, 412);
            this.Controls.Add(this.cmdShowErrors);
            this.Controls.Add(this.lblErrors);
            this.Controls.Add(this.openFolderButton);
            this.Controls.Add(this.colourFlowPanel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textDisplayTracker);
            this.Controls.Add(this.fileInformationLabel);
            this.Controls.Add(this.treemapControl);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.folderInput);
            this.Controls.Add(this.goButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FileTreeDisplayForm";
            this.Text = "File TreeMap Viewer";
            ((System.ComponentModel.ISupportInitialize)(this.textDisplayTracker)).EndInit();
            this.nodeMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button cmdShowErrors;
        private System.Windows.Forms.FlowLayoutPanel colourFlowPanel;
        private System.Windows.Forms.Label fileInformationLabel;
        private System.Windows.Forms.TextBox folderInput;
        private System.Windows.Forms.Button goButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblErrors;
        private System.Windows.Forms.ToolStripMenuItem nodeMenuExplore;
        private System.Windows.Forms.ToolStripMenuItem nodeMenuOpen;
        private System.Windows.Forms.ContextMenuStrip nodeMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem nodeMenuZoomIn;
        private System.Windows.Forms.ToolStripMenuItem nodeMenuZoomOut;
        private System.Windows.Forms.Button openFolderButton;
        private System.Windows.Forms.TrackBar textDisplayTracker;
        private Microsoft.Research.CommunityTechnologies.Treemap.TreemapControl treemapControl;

        #endregion
    }
}

