# FileTreeView

This is an old, but still handy (and still working!) utility that can trawl a drive or 
folder and display its contents in a tree map.

That's really useful to find out what is eating your disk space, whether it's a single gigantic file or 
a folder containing a million tiny files.

It uses some old Microsoft controls (treemapcontrol.dll and treemapgenerator.dll) so it is reolutely stuck in 32bitland.

If anyone fancies replacing the treemap control with something modern then have at it!


![](screenshot.png)