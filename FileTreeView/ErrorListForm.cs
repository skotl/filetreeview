﻿using System;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FileTreeView
{
    public partial class ErrorListForm : Form
    {
        private readonly ErrorList errorList;

        public ErrorListForm(ErrorList errorList)
        {
            this.errorList = errorList;
            InitializeComponent();
            SetupErrors();
        }

        private void SetupErrors()
        {
            var infoText = new StringBuilder();
            infoText.AppendFormat(@"{0} errors", errorList.ErrorCount);

            if (errorList.ErrorCount > errorList.MaxErrorConstraint)
                infoText.AppendFormat(@" (list is limited to first {0} entries)", errorList.MaxErrorConstraint);

            lblInfo.Text = infoText.ToString();
            lstErrors.DataSource = errorList.Errors.ToList();
        }

        private void cmdClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}