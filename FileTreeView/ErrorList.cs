﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace FileTreeView
{
    public class ErrorList
    {
        private const int MaxErrors = 1000;
        private readonly ConcurrentStack<FileOrDirectoryException> errors = new ConcurrentStack<FileOrDirectoryException>();
        private readonly object lockObject = new object();

        public int MaxErrorConstraint
        {
            get
            {
                return MaxErrors;
            }
        }
        
        public int UnLoggedErrorTotal { get; private set; }

        public int ErrorCount
        {
            get { return errors.Count + UnLoggedErrorTotal; }
        }

        public IEnumerable<string> Errors
        {
            get
            {
                return errors.ToList().Select(e => e.ToString());
            }
        }

        public void Add(FileOrDirectoryException ex)
        {
            lock (lockObject)
            {
                if (errors.Count >= MaxErrors)
                {
                    UnLoggedErrorTotal++;
                    return;
                }
            }

            errors.Push(ex);
        }
    }
}